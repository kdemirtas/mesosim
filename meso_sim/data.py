import networkx as nx


class Node:
    def __init__(self, kwargs):
        self.node_id = None
        self.__dict__.update(kwargs)

    def __repr__(self):
        return self.node_id


class Link:
    def __init__(self, kwargs):
        self.link_id = None
        self.__dict__.update(kwargs)

    def __repr__(self):
        return str(self.link_id)


class Segment:
    def __init__(self, kwargs):
        self.segment_id = None
        self.__dict__.update(kwargs)

    def __repr__(self):
        return str(self.segment_id)


class Network:
    def __init__(self, node_list, link_list, segment_list):
        self.node_list = node_list
        self.link_list = link_list
        self.segment_list = segment_list
        self.link_based_network = nx.DiGraph()
        self.lane_based_network = nx.DiGraph()

    def draw_network(self, network_type="lane_based"):
        if network_type == "lane_based":
            nx.draw(self.lane_based_network)
        else:
            nx.draw(self.link_based_network)
