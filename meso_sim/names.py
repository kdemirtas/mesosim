class Globals:
    def __init__(self, kwargs={}):
        self.__dict__.update(kwargs)
        self._create_num = 1
        self._create_name = "CREATE{}".format((self._create_num))

        self._process_num = 1
        self._process_name = "PROCESS{}".format((self._process_num))

    def _update_creates(self):
        self._create_num = self._create_num + 1
        self._create_name = "CREATE{}".format((self._create_num))

    def _update_processes(self):
        self._process_num = self._process_num + 1
        self._process_name = "PROCESS{}".format((self._process_num))

    def update_globals(self):
        self._update_creates()
        self._update_processes()

    def add_global(self, name):
        self.name = "{}".format((name))
