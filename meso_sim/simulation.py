import numpy as np

PROCESS_ACTION = {
    1: "Delay",
    2: "Seize_Delay",
    3: "Seize_Delay_Release",
    4: "Delay_Release"
}

DISTRIBUTION = {
    1: "Constant",
    2: "Expo",
    3: "Normal",
    4: "Triangular",
    5: "Expression"
}


class Entity:
    def __init__(self, env, entity_type, entity_picture, report_statistics=True, kwargs=None, distribution="constant", stdev=None, minimum=None, maximum=None, unit="hours", value=1):
        self.env = env
        self.entity_type = entity_type
        self.report_statistics = report_statistics
        self.origin = None
        self.destination = None
        self.entity_id = None
        self.current_node = None
        self.arrival_time = None
        self.time_in_system = 0.0
        self.tentative_path = None
        self.distribution = distribution
        self.stdev = stdev
        self.minimum = minimum
        self.maximum = maximum
        self.unit = unit
        self.value = value
        self.update_attributes(kwargs)
        self.do = env.process(self.create())

    def __repr__(self):
        return str(self.entity_type)

    def create(self):
        while True:
            delay_time = self._calculate_interarrival_time()
            print("Vehicle created at time {}".format(self.env.now))
            yield self.env.timeout(delay_time)

    def _calculate_interarrival_time(self):
        if self.distribution == "constant":
            mean = self.value
            delay_time = mean
        elif self.distribution == "expo":
            mean = self.value
            delay_time = np.random.exponential(mean)
            # TODO check rate vs mean
        elif self.distribution == "normal":
            mean = self.value
            stdev = self.stdev
            delay_time = np.random.normal(mean, stdev)
        elif self.distribution == "triangular":
            mean = self.value
            minimum = self.minimum
            maximum = self.maximum
            # TODO calculate from triangular
        else:
            raise(ValueError("Distribution not supported yet"))

        return delay_time

    def update_attributes(self, kwargs):
        self.__dict__.update(kwargs)


class Attribute:
    def __init__(self, env, name="Attribute 1", comment="", n_rows=0, n_cols=0, data_type=float, initial_values=None):
        self.env = env
        self.name = name
        self.comment = comment
        self.n_rows = n_rows
        self.n_cols = n_cols
        self.data_type = data_type
        self.initial_values = initial_values

    def __repr__(self):
        return str(self.name)


class Variable:
    def __init__(self, env, name="Attribute 1", comment="", n_rows=0, n_cols=0, data_type=float, initial_values=None, report_statistics=True):
        self.env = env
        self.name = name
        self.comment = comment
        self.n_rows = n_rows
        self.n_cols = n_cols
        self.data_type = data_type
        self.initial_values = initial_values
        self.report_statistics = report_statistics

    def __repr__(self):
        return str(self.name)


class ProcessProcess:
    def __init__(self, env, name="Process 1", process_type="standard", action="delay", delay_type="constant", stdev=None, minimum=None, maximum=None, unit="hours", value=1):
        self.env = env
        self.name = name
        self.process_type = process_type
        self.action = action
        self.delay_type = delay_type
        self.value = value
        # TODO check action and make sure it is one of the expressions defined
        # top of this file under PROCESS_ACTION

    def __repr__(self):
        return (self.name)

    def process(self):
        pass
