import simpy

from meso_sim import simulation as sim


env = simpy.Environment()
kwargs = {
    "origin": "1",
    "destination": "2",
    "vehicle_id": 1,
    "current_node": "1",
    "arrival_time": 0.0,
    "time_in_system": 0.0,
    "tentative_path": None
}
entity = sim.Entity(env, entity_type="Vehicle", entity_picture=None, kwargs=kwargs)
env.run(until=5)

print("done")
