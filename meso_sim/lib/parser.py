import json
import os
from meso_sim import data

import numpy as np


class DataParser:
    def __init__(self, input):
        self.input = input
        self.root_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        self.data_dir = os.path.join(self.root_dir, "data")

    def _parse_settings(self):
        settings = self.input.get("settings")
        if settings is None:
            raise UserWarning("Caution required by user. No settings provided")

        return settings

    def _parse_nodes(self):
        nodes = self.input.get("nodes")
        if nodes is None:
            raise UserWarning("Caution required by user. Nodes are not provided")

        node_list = []
        for node in nodes:
            n = data.Node(node)
            node_list.append(n)

        return nodes, node_list

    def _parse_links(self):
        links = self.input.get("links")
        if links is None:
            raise UserWarning("Caution required by user. Nodes are not provided")

        link_list = []
        for link in links:
            li = data.Link(link)
            link_list.append(li)

        return links, link_list

    def _parse_segments(self):
        segments = self.input.get("segments")
        if segments is None:
            raise UserWarning("Caution required by user. Segments are not provided")

        segment_list = []
        for segment in segments:
            s = data.Segment(segment)
            segment_list.append(s)

        return segments, segment_list

    def _parse_demand(self):
        demand = self.input.get("demand")
        if demand is None:
            raise UserWarning("Caution required by user. Demand is not provided")

        return demand

    def parse(self):
        settings = self._parse_settings()
        nodes, node_list = self._parse_nodes()
        links, link_list = self._parse_links()
        segments, segment_list = self._parse_segments()
        demand = self._parse_demand()

        result = {
            "settings": settings,
            "nodes": nodes,
            "node_list": node_list,
            "links": links,
            "link_list": link_list,
            "segments": segments,
            "segment_list": segment_list,
            "demand": demand
        }

        return result
