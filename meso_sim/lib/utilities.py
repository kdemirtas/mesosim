import numpy as np


class FundamentalDiagram:
    def __init__(self, v_free, q_max, k_jam):
        self.v_free = v_free
        self.q_max = q_max
        self.k_jam = k_jam

    def Newell_triangular(self, k):
        self.k_critical = self.q_max / self.v_free
        w = -((self.v_free * self.k_critical) / (self.k_jam - self.k_critical))
        q = min(self.v_free * k, w * (k - self.k_jam))

        result = {
            "k_critical": self.k_critical,
            "q": q,
            "w": w
        }

        return result


def mile_to_km(mile):
    return mile * 1.60934


def find_node_by_node_id(node_list, node_id):
    node = next(item for item in node_list if item.node_id == node_id)

    return node


def find_segment_by_segment_id(segment_list, segment_id):
    segment = next(item for item in segment_list if item.segment_id == segment_id)

    return segment


def find_link_by_link_id(link_list, link_id):
    link = next(item for item in link_list if item.link_id == link_id)

    return link
