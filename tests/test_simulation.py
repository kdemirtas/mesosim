import os
import unittest
import json
import simpy

from meso_sim import simulation as sim


class TestSimulation(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_entity_object_gets_created(self):
        env = simpy.Environment()
        kwargs = {
            "origin": "1",
            "destination": "2",
            "vehicle_id": 1,
            "current_node": "1",
            "arrival_time": 0.0,
            "time_in_system": 0.0,
            "tentative_path": None
        }
        entity = sim.Entity(env, entity_type="Vehicle", entity_picture=None, kwargs=kwargs)
        self.assertEqual(entity.entity_type, "Vehicle")

    def test_entity_object_creates_entities(self):
        env = simpy.Environment()
        kwargs = {
            "origin": "1",
            "destination": "2",
            "vehicle_id": 1,
            "current_node": "1",
            "arrival_time": 0.0,
            "time_in_system": 0.0,
            "tentative_path": None
        }
        entity = sim.Entity(env, entity_type="Vehicle", entity_picture=None, kwargs=kwargs)
        env.run(until=5)
        self.assertEqual(entity.entity_type, "Vehicle")
        print("done")

    def test_entity_object_creates_entities_exponential(self):
        env = simpy.Environment()
        kwargs = {
            "origin": "1",
            "destination": "2",
            "vehicle_id": 1,
            "current_node": "1",
            "arrival_time": 0.0,
            "time_in_system": 0.0,
            "tentative_path": None,
            "distribution": "expo",
            "value": 50
        }
        entity = sim.Entity(env, entity_type="Vehicle", entity_picture=None, kwargs=kwargs)
        env.run(until=500)
        self.assertEqual(entity.entity_type, "Vehicle")
        print("done")

    def test_entity_object_creates_entities_normal(self):
        env = simpy.Environment()
        kwargs = {
            "origin": "1",
            "destination": "2",
            "vehicle_id": 1,
            "current_node": "1",
            "arrival_time": 0.0,
            "time_in_system": 0.0,
            "tentative_path": None,
            "distribution": "expo",
            "value": 10,
            "stdev": 5
        }
        entity = sim.Entity(env, entity_type="Vehicle", entity_picture=None, kwargs=kwargs)
        env.run(until=500)
        self.assertEqual(entity.entity_type, "Vehicle")
        print("done")


if __name__ == "__main__":
        unittest.main()
