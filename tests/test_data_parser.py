import os
import unittest
import json

import networkx as nx
import matplotlib.pyplot as plt

from meso_sim.lib import parser
from meso_sim import data


class TestDataParser(unittest.TestCase):

    def setUp(self):
        self.root_dir = os.path.dirname(os.path.dirname(__file__))
        self.test_dir = os.path.join(self.root_dir, "tests")
        self.test_data = os.path.join(self.test_dir, "test_data", 'input_data.json')
        with open(self.test_data) as fp:
            self.input_data = json.load(fp)
        self.parser = parser.DataParser(self.input_data)

    def tearDown(self):
        pass

    def test_parser_settings(self):
        settings = self.parser._parse_settings()
        self.assertIsNotNone(settings)

    def test_parser_nodes(self):
        nodes, node_list = self.parser._parse_nodes()
        self.assertIsNotNone(nodes)
        self.assertTrue(len(nodes) == 6)
        self.assertIsNotNone(node_list)
        self.assertTrue(len(node_list) == 6)

    def test_parser_links(self):
        links, link_list = self.parser._parse_links()
        self.assertIsNotNone(links)
        self.assertTrue(len(links) == 1)
        self.assertIsNotNone(link_list)
        self.assertTrue(len(link_list) == 1)

    def test_parser_segments(self):
        segments, segment_list = self.parser._parse_segments()
        self.assertIsNotNone(segments)
        self.assertTrue(len(segments) == 8)
        self.assertIsNotNone(segment_list)
        self.assertTrue(len(segment_list) == 8)

    def test_data_network_link_based(self):
        result = self.parser.parse()
        node_list = result["node_list"]
        link_list = result["link_list"]
        links = result["links"]
        segment_list = result["segment_list"]
        network = data.Network(node_list, link_list, segment_list)
        for link in link_list:
            tail = link.tail
            tail_node = next(item for item in node_list if item.node_id == tail)
            head = link.head
            head_node = next(item for item in node_list if item.node_id == head)
            link_attributes = next(item for item in links if item["link_id"] == link.link_id)
            network.link_based_network.add_edge(tail_node, head_node, **link_attributes)

        self.assertListEqual(list(network.link_based_network.nodes), [tail_node, head_node])

    def test_data_network_lane_based(self):
        result = self.parser.parse()
        node_list = result["node_list"]
        link_list = result["link_list"]
        segment_list = result["segment_list"]
        segments = result["segments"]
        network = data.Network(node_list, link_list, segment_list)
        for segment in segment_list:
            tail = segment.tail
            tail_node = next(item for item in node_list if item.node_id == tail)
            head = segment.head
            head_node = next(item for item in node_list if item.node_id == head)
            segment_attributes = next(item for item in segments if item["segment_id"] == segment.segment_id)
            network.lane_based_network.add_edge(tail_node, head_node, **segment_attributes)

        self.assertEqual(network.lane_based_network.number_of_nodes(), 6)
        self.assertEqual(network.lane_based_network.number_of_edges(), 8)

    def test_draw_data_network_lane_based(self):
        result = self.parser.parse()
        node_list = result["node_list"]
        link_list = result["link_list"]
        segment_list = result["segment_list"]
        segments = result["segments"]
        network = data.Network(node_list, link_list, segment_list)
        for segment in segment_list:
            tail = segment.tail
            tail_node = next(item for item in node_list if item.node_id == tail)
            head = segment.head
            head_node = next(item for item in node_list if item.node_id == head)
            segment_attributes = next(item for item in segments if item["segment_id"] == segment.segment_id)
            network.lane_based_network.add_edge(tail_node, head_node, **segment_attributes)

        nx.draw(network.lane_based_network)
        plt.show()


if __name__ == "__main__":
        unittest.main()
