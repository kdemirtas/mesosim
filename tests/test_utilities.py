import os
import unittest
import json

import networkx as nx
import matplotlib.pyplot as plt

from meso_sim.lib import parser
from meso_sim.lib import utilities


class TestUtilities(unittest.TestCase):

    def setUp(self):
        self.root_dir = os.path.dirname(os.path.dirname(__file__))
        self.test_dir = os.path.join(self.root_dir, "tests")
        self.test_data = os.path.join(self.test_dir, "test_data", 'input_data.json')
        with open(self.test_data) as fp:
            self.input_data = json.load(fp)
        self.parser = parser.DataParser(self.input_data)
        self.result = self.parser.parse()

    def tearDown(self):
        pass

    def test_find_node_by_node_id(self):
        node_id = "2"
        node = utilities.find_node_by_node_id(self.result["node_list"], node_id)
        self.assertEqual(node.node_id, node_id)

    def test_find_link_by_link_id(self):
        link_id = 1
        link = utilities.find_link_by_link_id(self.result["link_list"], link_id)
        self.assertEqual(link.link_id, link_id)

    def test_find_segment_by_segment_id(self):
        segment_id = 7
        segment = utilities.find_segment_by_segment_id(self.result["segment_list"], segment_id)
        self.assertEqual(segment.segment_id, segment_id)


if __name__ == "__main__":
        unittest.main()
